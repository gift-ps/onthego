package com.giftps.onthego;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = "EmailPassword";
    private @ServerTimestamp
    Date timestamp;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    EditText mInputEmail, mInputPassword, mUserName, mUserBday, mUserTown;
    Button mBtnRegister;
    TextView mLogIn;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mInputEmail = (EditText) findViewById(R.id.inputEmail);
        mInputPassword = (EditText) findViewById(R.id.inputPassword);
        mBtnRegister = (Button) findViewById(R.id.btnRegister);
        mUserName = (EditText) findViewById(R.id.userName);
        mUserTown = (EditText) findViewById(R.id.userTown);
        mUserBday = (EditText) findViewById(R.id.userBday);
        progressBar = findViewById(R.id.progressBar);

        // Initialize Firebase Auth and cloud firestore
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();


        FirebaseUser getCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        if(getCurrentUser !=null){
            Toast.makeText(SignUpActivity.this, "You are logged in",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(SignUpActivity.this, HomeActivity.class));
        }

        registerNewUser();
        logUserIn();
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();

    }

    private void registerNewUser() {
        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mInputEmail.getText().toString();
                String pwd = mInputPassword.getText().toString();
                if(email.isEmpty()){
                    mInputEmail.setError("Please enter email id");
                    mInputEmail.requestFocus();
                }
                else  if(pwd.isEmpty()){
                    mInputPassword.setError("Please enter your password");
                    mInputPassword.requestFocus();
                }
                else  if(email.isEmpty() && pwd.isEmpty()){
                    Toast.makeText(SignUpActivity.this,"Fields Are Empty!",Toast.LENGTH_SHORT).show();
                }
                else  if(!(email.isEmpty() && pwd.isEmpty())){

                    progressBar.setVisibility(View.VISIBLE);
                    //Create new user
                    mAuth.createUserWithEmailAndPassword(email, pwd)
                            .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d(TAG, "createUserWithEmail:success");
                                        //////FirebaseUser user = mAuth.getCurrentUser();
                                        /////startActivity(new Intent(SignUpActivity.this, HomeActivity.class));
                                        saveUserToFirestore();
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                        Toast.makeText(SignUpActivity.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                }
                else{
                    Toast.makeText(SignUpActivity.this,"Error Occurred!",Toast.LENGTH_SHORT).show();

                }

            }
        });
    }

    private void saveUserToFirestore() {

        String email = mInputEmail.getText().toString();
        String names = mUserName.getText().toString();
        String town = mUserTown.getText().toString();
        String bDay = mUserBday.getText().toString();
        String pwd = mInputPassword.getText().toString();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String userId = user.getUid();

        // Create a new user
        Map<String, Object> newUser = new HashMap<>();
        newUser.put("names", names);
        newUser.put("email", email);
        newUser.put("town", town);
        newUser.put("birthday", bDay);
        newUser.put("pwd", pwd);
        newUser.put("uID", userId);

        // Add a new user with a generated ID
        db.collection("users").document(names)
                .set(newUser)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "User successfully registered");
                    }

                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });

        progressBar.setVisibility(View.INVISIBLE);

        //FirebaseUser user = mAuth.getCurrentUser();
        startActivity(new Intent(SignUpActivity.this, HomeActivity.class));


    }
    // [END on_start_check_user]

    private void logUserIn() {
        mLogIn = findViewById(R.id.logIn);
        mLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
            }
        });
    }

    private void createAccount(String email, String password) {

    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mInputEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mInputEmail.setError("Required.");
            valid = false;
        } else {
            mInputEmail.setError(null);
        }

        String password = mInputPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mInputPassword.setError("Required.");
            valid = false;
        } else {
            mInputPassword.setError(null);
        }

        return valid;
    }

}

package com.giftps.onthego.driver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.giftps.onthego.MainActivity;
import com.giftps.onthego.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class DriverHomeActivity extends AppCompatActivity {

    private static final String TAG = "OnTheGo! Database";
    TextView showEmail, showName, showTown, showBday, showPhnNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_home);

        displayDriverInfo();
    }

    private void displayDriverInfo() {

        showName = findViewById(R.id.driverName);
        showEmail = findViewById(R.id.driverEmail);
        showTown = findViewById(R.id.driverTown);
        showBday = findViewById(R.id.driverBday);
        showPhnNumber = findViewById(R.id.driverPhnNumber);
        // [START get_user_profile]
        FirebaseUser driver = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if (driver != null) {
            String driverId = driver.getUid();

            db.collection("drivers")
                    .whereEqualTo("uID",driverId)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                for (DocumentSnapshot document : task.getResult()) {
                                    //email = (String) document.get("name");
                                    //showEmail.setText(task.getResult().get);
                                    showName.setText((CharSequence) document.get("names"));
                                    showEmail.setText((String) document.get("email"));
                                    showTown.setText((CharSequence) document.get("town"));
                                    showBday.setText((CharSequence) document.get("birthday"));
                                    showPhnNumber.setText((CharSequence) document.get("phoneNumber"));
                                }
                            }
                        }
                    });
        }
    }

    public void logUserOut(View view) {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}

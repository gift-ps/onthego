package com.giftps.onthego.driver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.giftps.onthego.HomeActivity;
import com.giftps.onthego.LoginActivity;
import com.giftps.onthego.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class DriverLoginActivity extends AppCompatActivity {

    private static final String TAG = "DriverLogIn";
    private FirebaseAuth mAuth;
    EditText mInputEmail, mInputPassword;
    Button mBtnRegister;
    //TextView tvSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_login);


        mInputEmail = findViewById(R.id.email);
        mInputPassword = findViewById(R.id.password);
        mBtnRegister = findViewById(R.id.button);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser getCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        if(getCurrentUser != null){
            Toast.makeText(DriverLoginActivity.this, "You are logged in",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(DriverLoginActivity.this, DriverHomeActivity.class));
        }

        logDriverIn();
    }

    private void logDriverIn() {

        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mInputEmail.getText().toString();
                String pwd = mInputPassword.getText().toString();
                if (email.isEmpty()) {
                    mInputEmail.setError("Please enter your email");
                    mInputEmail.requestFocus();
                } else if (pwd.isEmpty()) {
                    mInputPassword.setError("Please enter your password");
                    mInputPassword.requestFocus();
                } else if (email.isEmpty() && pwd.isEmpty()) {
                    Toast.makeText(DriverLoginActivity.this, "Fields Are Empty!", Toast.LENGTH_SHORT).show();
                } else if (!(email.isEmpty() && pwd.isEmpty())) {

                    mAuth.signInWithEmailAndPassword(email, pwd)
                            .addOnCompleteListener(DriverLoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d(TAG, "signInWithEmail:success");
                                        //FirebaseUser user = mAuth.getCurrentUser();
                                        startActivity(new Intent(DriverLoginActivity.this, DriverHomeActivity.class));
                                        //updateUI(user);
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                                        Toast.makeText(DriverLoginActivity.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                        //updateUI(null);
                                    }
                                }
                            });

                } else {
                    Toast.makeText(DriverLoginActivity.this, "Error Occurred!", Toast.LENGTH_SHORT).show();

                }

            }
        });

    }

}

package com.giftps.onthego.driver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.giftps.onthego.HomeActivity;
import com.giftps.onthego.LoginActivity;
import com.giftps.onthego.R;
import com.giftps.onthego.SignUpActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DriverSignUpActivity extends AppCompatActivity {

    private static final String TAG = "EmailPassword";
    private @ServerTimestamp Date timestamp;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    EditText mDriverEmail, mDriverPassword, mDriverName, mDriverBday, mDriverTown, mDriverPhoneNunber;
    Button mBtnRegister;
    TextView mLogIn;
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_sign_up);

        mDriverEmail = (EditText) findViewById(R.id.driverEmail);
        mDriverPassword = (EditText) findViewById(R.id.driverPassword);
        mBtnRegister = (Button) findViewById(R.id.btnRegister);
        mDriverName = (EditText) findViewById(R.id.driverName);
        mDriverTown = (EditText) findViewById(R.id.driverTown);
        mDriverBday = (EditText) findViewById(R.id.driverBday);
        mDriverPhoneNunber = (EditText) findViewById(R.id.driverPhoneNumber);
        mProgressBar = findViewById(R.id.progressBar);

        // Initialize Firebase Auth and cloud firestore
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();


    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser checkCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        if(checkCurrentUser !=null){
            Toast.makeText(DriverSignUpActivity.this, "You are logged in",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(DriverSignUpActivity.this, DriverHomeActivity.class));
        }

        registerNewDriver();

        mLogIn = (TextView) findViewById(R.id.signIn);
        mLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DriverSignUpActivity.this, DriverLoginActivity.class));
            }
        });
    }

    private void registerNewDriver() {
        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mDriverEmail.getText().toString();
                String pwd = mDriverPassword.getText().toString();
                if(email.isEmpty()){
                    mDriverEmail.setError("Please enter email id");
                    mDriverEmail.requestFocus();
                }
                else  if(pwd.isEmpty()){
                    mDriverPassword.setError("Please enter your password");
                    mDriverPassword.requestFocus();
                }
                else  if(email.isEmpty() && pwd.isEmpty()){
                    Toast.makeText(DriverSignUpActivity.this,"Fields Are Empty!",Toast.LENGTH_SHORT).show();
                }
                else  if(!(email.isEmpty() && pwd.isEmpty())){

                    showDialog();
                    //Create new user
                    mAuth.createUserWithEmailAndPassword(email, pwd)
                            .addOnCompleteListener(DriverSignUpActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "createUserWithEmail:success");

                                        saveDriverToFirestore();
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                        Toast.makeText(DriverSignUpActivity.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                    hideDialog();
                }
                else{
                    Toast.makeText(DriverSignUpActivity.this,"Unknown error Occurred!",Toast.LENGTH_SHORT).show();

                }

            }
        });
    }

    private void saveDriverToFirestore() {

        String email = mDriverEmail.getText().toString();
        String names = mDriverName.getText().toString();
        String town = mDriverTown.getText().toString();
        String bDay = mDriverBday.getText().toString();
        String pwd = mDriverPassword.getText().toString();
        String phnNumber = mDriverPhoneNunber.getText().toString();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String userId = user.getUid();

        // Create a new driver
        Map<String, Object> newDriver = new HashMap<>();
        newDriver.put("names", names);
        newDriver.put("email", email);
        newDriver.put("town", town);
        newDriver.put("birthday", bDay);
        newDriver.put("pwd", pwd);
        newDriver.put("phoneNumber", phnNumber);
        newDriver.put("uID", userId);

        // Add a new document with a generated ID
        db.collection("drivers").document(names)
                .set(newDriver)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "Driver successfully registered");
                    }

                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding new driver to database", e);
                    }
                });

        startActivity(new Intent(DriverSignUpActivity.this, DriverHomeActivity.class));

    }

    private void showDialog(){
        mProgressBar.setVisibility(View.VISIBLE);

    }

    private void hideDialog(){
        if(mProgressBar.getVisibility() == View.VISIBLE){
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

}

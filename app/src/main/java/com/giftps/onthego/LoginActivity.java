package com.giftps.onthego;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "EmailPassword";
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseAuth mAuth;
    EditText mInputEmail, mInputPassword;
    Button mBtnRegister;
    ProgressBar progressBar;
    TextView tvCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

            mInputEmail = findViewById(R.id.email);
            mInputPassword = findViewById(R.id.password);
            mBtnRegister = findViewById(R.id.button);
            progressBar = findViewById(R.id.progressBar);

            // Initialize Firebase Auth
            FirebaseUser checkCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
            if(checkCurrentUser != null){
                Toast.makeText(LoginActivity.this, "You are logged in",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            }

        logUserIn();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void logUserIn() {

        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mInputEmail.getText().toString();
                String pwd = mInputPassword.getText().toString();
                if (email.isEmpty()) {
                    mInputEmail.setError("Please enter your email");
                    mInputEmail.requestFocus();
                } else if (pwd.isEmpty()) {
                    mInputPassword.setError("Please enter your password");
                    mInputPassword.requestFocus();
                } else if (email.isEmpty() && pwd.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Fields Are Empty!", Toast.LENGTH_SHORT).show();
                } else if (!(email.isEmpty() && pwd.isEmpty())) {

                    progressBar.setVisibility(View.VISIBLE);

                    mAuth.signInWithEmailAndPassword(email, pwd)
                            .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "signInWithEmail:success");
                                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                                        //updateUI(user);
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                                        Toast.makeText(LoginActivity.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                        //updateUI(null);
                                    }
                                }
                            });
                    progressBar.setVisibility(View.GONE);

                } else {
                    Toast.makeText(LoginActivity.this, "Error Occurred!", Toast.LENGTH_SHORT).show();

                }

            }
        });

    }

    public void createUserInstead(View view) {
        startActivity(new Intent(this, SignUpActivity.class));
    }
}

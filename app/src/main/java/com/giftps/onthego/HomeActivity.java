package com.giftps.onthego;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = "OnTheGo! Database";
    TextView showEmail, showName, showTown, showBday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        displayUserInfo();

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void displayUserInfo() {

        showName = findViewById(R.id.displayName);
        showEmail = findViewById(R.id.displayEmail);
        showTown = findViewById(R.id.displayTown);
        showBday = findViewById(R.id.displayBday);
        // [START get_user_profile]
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //////////showEmail.setText(task.getResult().get);
        if (user != null) {
            String userId = user.getUid();

            db.collection("users")
                    .whereEqualTo("uID",userId)//looks for the corresponding value with the field
                    // in the database
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                for (DocumentSnapshot document : task.getResult()) {
                                    //email = (String) document.get("name");
                                    //showEmail.setText(task.getResult().get);
                                    showName.setText((CharSequence) document.get("names"));
                                    showEmail.setText((String) document.get("email"));
                                    showTown.setText((CharSequence) document.get("town"));
                                    showBday.setText((CharSequence) document.get("birthday"));

                                    // These values must exactly match the fields you have in your db

                                }
                            }
                        }
                    });
        }
    }

    public void logUserOut(View view) {
        FirebaseAuth user = FirebaseAuth.getInstance();
        user.signOut();
    }
}
